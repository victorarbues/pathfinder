﻿
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Formatters;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.PlatformAbstractions;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using PathFinder.Api.Modules;
using System;

namespace PathFinder.Api
{
	public class Startup
	{
		private IConfigurationRoot Config;
		private IContainer _container;

		public Startup(IHostingEnvironment env, IApplicationEnvironment appEnv)
		{
			var builder = new ConfigurationBuilder()
				  .SetBasePath(appEnv.ApplicationBasePath)
				  .AddJsonFile("config.json");
			Config = builder.Build();
		}

		public IServiceProvider ConfigureServices(IServiceCollection services)
		{
			services.AddMvc().AddJsonOptions(options => {
				options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
				options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
				options.SerializerSettings.Formatting = Formatting.Indented;
				options.SerializerSettings.Converters.Add(new Newtonsoft.Json.Converters.StringEnumConverter());
			});
			services.AddCors();

			// Create the autofac container
			var builder = new ContainerBuilder();

			builder.RegisterInstance(Config);

			// Add any Autofac modules or registrations.
			builder.RegisterModule<PathFinderModule>();
			builder.Populate(services);

			// Build the container.
			_container = builder.Build();

			// Resolve and return the service provider.
			return _container.Resolve<IServiceProvider>();
		}

		public void Configure(IApplicationBuilder app, ILoggerFactory loggerFactory)
		{
			app.Use(async (context, next) =>
			{
				context.Response.Headers.Add("Access-Control-Allow-Origin", "*");
				context.Response.Headers.Add("Access-Control-Allow-Headers", "Content-type");
				if (context.Request.Method == "OPTIONS")
				{
					context.Response.StatusCode = 200;
				}
				else
				{
					await next();
				}
			});
			app.UseMvc();
		}
	}
}
