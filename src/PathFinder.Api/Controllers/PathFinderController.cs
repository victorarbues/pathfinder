﻿using Microsoft.AspNet.Mvc;
using PathFinder.Api.Requests;
using System.Threading.Tasks;
using PathFinder.Api.Commands;
using PathFinder.Api.Responses;

namespace PathFinder.Api.Controllers
{
	[Route("[controller]")]
	public class PathFinderController : Controller
	{
		private readonly ICommand<GetPathRequest, GetPathResponse> _getPathCommand;
		private readonly ICommand<SetMapRequest, SetMapResponse> _setMapCommand;

		public PathFinderController(ICommand<GetPathRequest, GetPathResponse> getPathCommand,
									ICommand<SetMapRequest, SetMapResponse> setMapCommand)
		{
			_getPathCommand = getPathCommand;
			_setMapCommand = setMapCommand;
		}

		public async Task<IActionResult> Get(GetPathRequest request)
		{
			try
			{
				return Json(await _getPathCommand.Execute(request));
			}
			catch
			{
				return HttpBadRequest();
			}
		}

		[HttpPut]
		public async Task<IActionResult> Put([FromBody]SetMapRequest request)
		{
			try
			{
				return Json(await _setMapCommand.Execute(request));
			}
			catch
			{
				return HttpBadRequest();
			}
		}
	}
}
