﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PathFinder.Api.Responses
{
    public class GetPathResponse : BaseResponse
    {

		public IList<Path> Paths { get; set; }

		public GetPathResponse() : base()
		{
			Paths = new List<Path>();
		}

    }
}
