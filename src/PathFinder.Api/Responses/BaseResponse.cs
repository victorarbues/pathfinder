﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PathFinder.Api.Responses
{
    public abstract class BaseResponse
    {

		public BaseResponse()
		{
			Result = ResultType.Ok.ToString();
		}

		public string Result { get; set; }
		public string Message { get; set; }
    }

	public enum ResultType
	{
		Ok,
		Error
	}

}
