﻿using PathFinder.Api.Requests;
using PathFinder.Api.Responses;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace PathFinder.Api.Commands
{
	public class GetPathCommand : ICommand<GetPathRequest, GetPathResponse>
	{

		private const string NoSuchRoute = "NO SUCH ROUTE";
		private const string RoutesFound = "Routes found";

		private readonly IPathFinder _pathFinder;

		public GetPathCommand(IPathFinder pathFinder)
		{
			_pathFinder = pathFinder;
		}

		public async Task<GetPathResponse> Execute(GetPathRequest request)
		{
			try
			{
				var paths = await _pathFinder.Paths(request.Academies, request.MaxStops, request.PathFinder.ToString(), request.MaxDistance );
				return new GetPathResponse
				{
					Paths = paths.ToList(),
					Message = !paths.Any() ? NoSuchRoute : RoutesFound
				};
			}
			catch (Exception ex)
			{
				return new GetPathResponse
				{
					Result = ResultType.Error.ToString(),
					Message = ex.Message
				};
			}
		}
	}
}
