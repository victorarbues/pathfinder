﻿using PathFinder.Api.Requests;
using PathFinder.Api.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PathFinder.Api.Commands
{
	public class SetMapCommand : ICommand<SetMapRequest, SetMapResponse>
	{

		private const string Success = "Map updated successfully";

		private readonly IPathFinder _pathFinder;

		public SetMapCommand(IPathFinder pathFinder)
		{
			_pathFinder = pathFinder;
		}

		public async Task<SetMapResponse> Execute(SetMapRequest request)
		{
			try
			{
				await _pathFinder.SetMap(request.Map);
				return new SetMapResponse
				{
					Message = Success
				};
			}
			catch (Exception ex)
			{
				return new SetMapResponse
				{
					Result = ResultType.Error.ToString(),
					Message = ex.Message
				};
			}
		}
	}
}
