﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PathFinder.Api.Commands
{
    public interface ICommand<TRequest, TResponse>
    {
		Task<TResponse> Execute(TRequest request);
    }
}
