﻿using Autofac;
using PathFinder.Api.Commands;

namespace PathFinder.Api.Modules
{
	public class PathFinderModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			var assembly = System.Reflection.Assembly.GetExecutingAssembly();

			builder.RegisterAssemblyTypes(assembly)
				.AsClosedTypesOf(typeof(ICommand<,>))
				.AsSelf();
			builder.RegisterType<PathFinder>().AsImplementedInterfaces().SingleInstance();
		}
	}
}
