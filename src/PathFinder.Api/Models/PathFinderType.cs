﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PathFinder.Api.Models
{
    public enum PathFinderType
    {
		Default,
		Circular,
		CircularLoop,
		Dijkstra,
		Direct,
		OriginDestination
    }
}
