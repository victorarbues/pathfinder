﻿using Microsoft.AspNet.Hosting;

namespace PathFinder.Api
{
	public static class Program
	{
		public static void Main(string[] args)
		{
			var application = new WebApplicationBuilder()
				.UseConfiguration(WebApplicationConfiguration.GetDefault(args))
				 .UseStartup("PathFinder.Api")
			   .Build();

			application.Run();
		}
	}
}