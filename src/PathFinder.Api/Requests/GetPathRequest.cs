﻿using PathFinder.Api.Models;

namespace PathFinder.Api.Requests
{
	public class GetPathRequest
	{
		public string Academies { get; set; }
		public PathFinderType PathFinder { get; set;}
		public int? MaxStops { get; set; }
		public int? MaxDistance { get; set; }
	}
}
