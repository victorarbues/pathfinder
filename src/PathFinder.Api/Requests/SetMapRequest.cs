﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PathFinder.Api.Requests
{
    public class SetMapRequest
    {
		public string Map { get; set; }
    }
}
