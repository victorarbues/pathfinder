﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PathFinder
{
	public class PathFinder : IPathFinder
	{

		private Map _map;

		//Just want the resolvers instantiated once
		private static readonly Lazy<PathResolvers> _pathResolvers =
			new Lazy<PathResolvers>(() => new PathResolvers());
		private static PathResolvers PathResolvers { get { return _pathResolvers.Value; } }

		public PathFinder() { }
		public PathFinder(string paths)
		{
			SetMap(paths).Wait();
		}

		public async Task SetMap(string paths)
		{
			await Task.Run(() => _map = new Map(paths));
		}

		public async Task<IEnumerable<Path>> Paths(string academies, int? maxStops = default(int?), string pathResolver = "default", int? maxDistance = default(int?))
		{
			if (_map == null) throw new ArgumentNullException("Map hasn't been setup.");

			return await PathResolvers[pathResolver.ToLower()].Resolve(_map, academies, maxStops, maxDistance);
		}
	}
}
