﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PathFinder
{
	internal interface IPathResolver
	{
		Task<IList<Path>> Resolve(Map map, string academies, int? maxStops = default(int?), int? maxDistance = default(int?));
	}
}
