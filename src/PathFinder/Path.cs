﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PathFinder
{
	public class Path
	{

		public Path()
		{
			Roads = new List<Road>();
		}

		public List<Road> Roads { private set; get; }

		public int Distance
		{
			get { return Roads.Sum(s => s.Distance); }
		}

		public override string ToString()
		{
			var road = string.Join(", ", Roads.Select(r => $"{r.From.Id} -> {r.To.Id}"));
			return $"{road}        Distance: {Distance}";
        }

	}
}
