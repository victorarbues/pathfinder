﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PathFinder
{
	/// <summary>
	/// Path resolver for circular paths.
	/// </summary>
	internal class CircularPathResolver : BasePathResolver
	{

		protected virtual bool StopWhenCompleted { get { return true; } }

		public override async Task<IList<Path>> Resolve(Map map, string academy, int? maxStops, int? maxDistance)
		{
			if (academy.Length != 1) throw new ArgumentException("For circular path resolvers only one academy should be specified");

			var paths = new List<Path>();

			Academy firstAndLastAcademy = map.Academies[academy.First()];
			await Visit(firstAndLastAcademy, firstAndLastAcademy, map, new List<Academy> { firstAndLastAcademy }, 0, 0, paths, maxStops, maxDistance);

			return paths.OrderBy(p => p.Distance).ToList();
		}

		protected async Task Visit(Academy goalAcademy, Academy currentAcademy, Map map, List<Academy> academiesVisited, int stop, int distance, IList<Path> paths, int? maxStops = default(int?), int? maxDistance = default(int?))
		{

			if (HasReachedEnd(currentAcademy, goalAcademy, academiesVisited))
			{
				paths.Add(BuildPath(map, academiesVisited));
				if(StopWhenCompleted) return;
			}

			foreach (var academy in currentAcademy.Connections)
			{
				var newDistance = distance + map.Roads[$"{currentAcademy.Id}{academy.Id}"].Distance;
				var newStop = stop + 1;
				if (CanContinue(maxStops, newStop, maxDistance, newDistance))
				{
					var newAcademiesVisited = academiesVisited.ConvertAll(a => a);
					newAcademiesVisited.Add(academy);
					await Visit(goalAcademy, academy, map, newAcademiesVisited, newStop, newDistance, paths, maxStops, maxDistance);
				}
			}
		}

		protected virtual bool HasReachedEnd(Academy goalAcademy, Academy currentAcademy, List<Academy> academiesVisited)
		{
			return goalAcademy == currentAcademy && academiesVisited.Count > 1;
		}
	}
}
