﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Reflection;
using System.Linq.Expressions;

namespace PathFinder
{
	/// <summary>
	/// This class allows to resolve to any existing or to be created path resolver
	/// </summary>
    internal class PathResolvers
    {

		private ILookup<string, IPathResolver> _pathResolvers;
		internal IPathResolver this[string pathResolverName]
		{
			get
			{
				var lookUpResolver = _pathResolvers[pathResolverName];
				if (lookUpResolver.Any())
				{
					return lookUpResolver.First();
				}
				throw new ArgumentException($"Path resolver with name {pathResolverName} not found.");
			}
		}

		public PathResolvers()
		{
			//All resolvers are instantiated and kept in the container _pathResolvers
			var pathResolverType = typeof(IPathResolver);
			var pathResolvers = GetType().Assembly.GetTypes().Where(t => pathResolverType.IsAssignableFrom(t) && !t.IsAbstract);
			_pathResolvers = pathResolvers.Select(pr => Activator.CreateInstance(pr) as IPathResolver)
										  .ToLookup(pr => pr.GetType().Name.ToLower().Replace("pathresolver",string.Empty));
		}
    }
}
