﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PathFinder
{
	/// <summary>
	/// Default path resolver. Returns a list of paths that go through all the specified academies.
	/// </summary>
	internal class DefaultPathResolver : BasePathResolver
	{

		public override async Task<IList<Path>> Resolve(Map map, string academies, int? maxStops, int? maxDistance)
		{
			var paths = new List<Path>();
			var academiesVisited = new List<Academy>();

			Academy initialAcademy = map.Academies[academies.First()];
			academiesVisited.Add(initialAcademy);
			await Visit(initialAcademy, map, academiesVisited, academies.Substring(1), 0, 0, paths, maxStops, maxDistance);

			return paths.OrderBy(p => p.Distance).ToList();
		}

		protected virtual async Task Visit(Academy currentAcademy, Map map, List<Academy> academiesVisited, string academiesLeft, int stop, int distance, IList<Path> paths, int? maxStops = default(int?), int? maxDistance = default(int?))
		{
			if (HasReachedEnd(currentAcademy, academiesVisited, academiesLeft))
			{
				paths.Add(BuildPath(map, academiesVisited));
				return;
			}

			//Remove current academy is it's one to visit
			if (academiesLeft.First() == currentAcademy)
			{
				academiesLeft = academiesLeft.Substring(1);
			}

			foreach (var academy in currentAcademy.Connections.Except(academiesVisited))
			{
				var newDistance = distance + map.Roads[$"{currentAcademy.Id}{academy.Id}"].Distance;
				var newStop = stop + 1;
				if (CanContinue(maxStops, newStop, maxDistance, newDistance))
				{
					var newAcademiesVisited = academiesVisited.ConvertAll(a => a);
					newAcademiesVisited.Add(academy);
					await Visit(academy, map, newAcademiesVisited, academiesLeft, newStop, newDistance, paths, maxStops, maxDistance);
				}
			}

		}

		protected virtual bool HasReachedEnd(Academy currentAcademy, List<Academy> academiesVisited, string academiesLeft)
		{
			// Has reached end if the current academy is the last one
			return academiesLeft.Length == 1 && currentAcademy == academiesLeft.Last();
		}

	}
}
