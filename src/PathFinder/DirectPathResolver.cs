﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PathFinder
{
	public class DirectPathResolver : BasePathResolver
	{
		public override async Task<IList<Path>> Resolve(Map map, string academies, int? maxStops = default(int?), int? maxDistance = default(int?))
		{
			var paths = new List<Path>();
			var path = new Path();
			var academiesVisited = new List<Academy>();

			for (var i = 0; i < academies.Length-i; i++)
			{
				Road road;
				if (!map.Roads.TryGetValue($"{academies[i]}{academies[i + 1]}", out road))
				{
					return paths;
				}
				path.Roads.Add(road);
			}

			paths.Add(path);
			return paths;
		}
	}
}
