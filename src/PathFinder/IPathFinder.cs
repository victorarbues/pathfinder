﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PathFinder
{
    public interface IPathFinder
    {
		Task<IEnumerable<Path>> Paths(string academies, int? maxStops = null, string pathResolver = "default", int? maxDistance = default(int?));
		Task SetMap(string paths);
	}
}
