﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PathFinder
{
	internal class DijkstraPathResolver : BasePathResolver
	{
		public override async Task<IList<Path>> Resolve(Map map, string academies, int? maxStops, int? maxDistance)
		{
			if (academies.Length != 2) throw new ArgumentException("Need to specify an origin and a destination (eg: AB)");

			var paths = new List<Path>();
			var path = new Path();
			Academy originAcademy = map.Academies[academies.First()];
			Academy destinationAcademy = map.Academies[academies.Last()];
			var academiesVisited = new List<Academy> { originAcademy };
			var distances = new Dictionary<Academy, Road>();

			await Visit(map, originAcademy, originAcademy, distances, academiesVisited, 0);

			path.Roads.Add(distances[destinationAcademy]);
			paths.Add(path);
			return paths;
		}

		private async Task Visit(Map map, Academy originAcademy, Academy currentAcademy, Dictionary<Academy, Road> distances, List<Academy> academiesVisited, int distance)
		{
			foreach (var academy in currentAcademy.Connections.Except(academiesVisited))
			{
				var road = map.Roads[$"{currentAcademy.Id}{academy.Id}"];
				var newDistance = road.Distance + distance;
				if (distances.ContainsKey(academy))
				{
					if (distances[academy].Distance > newDistance)
					{
						distances[academy] = new Road(newDistance, originAcademy, academy);
					}
				}
				else {
					distances[academy] = new Road(newDistance, originAcademy, academy);
				}
				var newAcademiesVisited = academiesVisited.ConvertAll(a => a);
				newAcademiesVisited.Add(academy);
				await Visit(map, originAcademy, academy, distances, newAcademiesVisited, newDistance);
			}
		}
	}
}
