﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PathFinder
{
	public abstract class BasePathResolver : IPathResolver
	{
		public abstract Task<IList<Path>> Resolve(Map map, string academies, int? maxStops = default(int?), int? maxDistance = default(int?));

		protected bool CanContinue(int? maxStops, int stops, int? maxDistance, int distance)
		{
			return (!maxStops.HasValue || stops <= maxStops.Value) && (!maxDistance.HasValue || distance < maxDistance.Value);
		}

		protected Path BuildPath(Map map, List<Academy> academiesVisited)
		{
			var path = new Path();

			for (var i = 0; i < academiesVisited.Count() - 1; i++)
			{
				var roadKey = new string(new[] { academiesVisited.ElementAt(i).Id, academiesVisited.ElementAt(i + 1).Id });
				path.Roads.Add(map.Roads[roadKey]);
			}
			return path;
		}
	}
}
