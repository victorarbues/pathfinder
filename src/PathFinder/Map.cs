﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace PathFinder
{
    public class Map
    {

		public IDictionary<string, Road> Roads { get; private set; }
		public IDictionary<char, Academy> Academies { get; private set; }

		public Map(string roads)
		{
			roads = roads.Replace(" ", string.Empty);
			if (!Regex.Match(roads, "^([a-zA-Z]{2}\\d+){1}(,{1}[a-zA-Z]{2}\\d+)+$").Success)
				throw new ArgumentException($"Input value is wrong. Expected a list of 2 letters and a number separated by a comma (e.g AB5, BC3, CD4). Got '{roads}'.");

			Roads = new Dictionary<string, Road>();
			Academies = new Dictionary<char, Academy>();
			Array.ForEach(roads.Split(','), s => {
				var key = s.Substring(0, 2);

				//Add road
				if (Roads.ContainsKey(key)) {
					throw new ArgumentException($"The road {key} has been passed twice");
				}
				Roads.Add(key, s);

				//Add/Update node with new connection 
				Academy academy;
				var academyKey = key.First();
				if (!Academies.TryGetValue(academyKey, out academy)) {
					academy = academyKey;
				}
				Academy connectionAcademy;
				var connectionAcademyKey = key.Last();
				if (!Academies.TryGetValue(connectionAcademyKey, out connectionAcademy))
				{
					connectionAcademy = connectionAcademyKey;
					Academies[connectionAcademy.Id] = connectionAcademy;
				}
				academy.Connections.Add(connectionAcademy);
				Academies[academy.Id] = academy;

			});

		}
    }
}
