﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PathFinder
{
	public class Academy
	{
		public ISet<Academy> Connections { private set; get; }

		public Academy(char id)
		{
			Id = id;
			Connections = new HashSet<Academy>();
		}

		public char Id { private set; get; }

		public static implicit operator Academy(char id)
		{
			return new Academy(id);
		}

		public static implicit operator char(Academy academy)
		{
			return academy.Id;
		}

    }
}
