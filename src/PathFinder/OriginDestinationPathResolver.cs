﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PathFinder
{
	internal class OriginDestinationPathResolver : BasePathResolver
	{

		public override async Task<IList<Path>> Resolve(Map map, string academies, int? maxStops, int? maxDistance)
		{
			if (academies.Length != 2) throw new ArgumentException("Need to specify an origin and a destination (eg: AB)");

			var paths = new List<Path>();
			var academiesVisited = new List<Academy>();

			Academy originAcademy = map.Academies[academies.First()];
			Academy destinationAcademy = map.Academies[academies.Last()];
			academiesVisited.Add(originAcademy);
			await Visit(originAcademy, map, academiesVisited, destinationAcademy, 0, 0, paths, maxStops, maxDistance);

			return paths.OrderBy(p => p.Distance).ToList();
		}

		protected virtual async Task Visit(Academy currentAcademy, Map map, List<Academy> academiesVisited, Academy destinationAcademy, int stop, int distance, IList<Path> paths, int? maxStops = default(int?), int? maxDistance = default(int?))
		{
			if (currentAcademy == destinationAcademy)
			{
				paths.Add(BuildPath(map, academiesVisited));
			}

			foreach (var academy in currentAcademy.Connections)
			{
				var newDistance = distance + map.Roads[$"{currentAcademy.Id}{academy.Id}"].Distance;
				var newStop = stop + 1;
				if (CanContinue(maxStops, newStop, maxDistance, newDistance))
				{
					var newAcademiesVisited = academiesVisited.ConvertAll(a => a);
					newAcademiesVisited.Add(academy);
					await Visit(academy, map, newAcademiesVisited, destinationAcademy, newStop, newDistance, paths, maxStops, maxDistance);
				}
			}

		}
	}
}
