﻿
namespace PathFinder
{
	/// <summary>
	/// Circular loop path resolver that doesn't stop once the origin academy is met
	/// </summary>
    internal class CircularLoopPathResolver : CircularPathResolver
	{
		protected override bool StopWhenCompleted { get { return false; } }
	}
}
