﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace PathFinder
{
    public class Road
	{
		public Road(int distance, Academy from, Academy to)
		{
			Distance = distance;
			From = from;
			To = to;
		}

		public int Distance { private set; get; }
		public Academy From { private set; get; }
		public Academy To { private set; get; }
		
		public static implicit operator Road(string road)
		{
			if (!Regex.Match(road, "^[a-zA-Z]{2}\\d+$").Success)
				throw new ArgumentException($"Input value is wrong. Expected 2 letters and a number. Got '{road}'.");

			Academy from = road.First();
			Academy to = road.ElementAt(1);
			var distance = Convert.ToInt32(road.Substring(2));

			return new Road(distance, from, to);
		}

	}
}
