# Path finder

This repository aims to solve the **TEACHER COMPUTER RETRIEVAL** programming problem.

## How to run it
For my own convinience, I decided to use `C#6` on the implementation of the solution.
This will require the reviewer to install the DNX environment.
This [manual](https://github.com/aspnet/Home) explains how to setup the environment. Once that is setup, `dnvm upgrade -u` might be needed as well, since the solution is using the latest versions.

I have included a test project and a web project in order to test the path finder library (didn't fancy to code a console app, so a web will do).

## How to test it
The projects need to have their nuget packages in place, to be built and to be run.
`dnu restore` and `dnu build` will short out the compilation.
`dnx test` will run the tests and `dnx web` will run the API site.
All these commands should be run under the folder with the `project.json` file.

When testing the API, there is just a couple of endpoints. The request can be found in the following table:

| Url           | Example Request           |   |
| ------------- |:-------------:| -----:|
| http://localhost:5001/pathfinder     | PUT { "Map": "AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7" } | Sets the map with the test values |
| http://localhost:5001/pathfinder      | GET ?Academies=AC&PathFinder=Default     |   Retrieves paths from A to C using the Default strategy |

These endpoints can be reach on http://cascomio.ddns.net:5001 if the server is still running on my machine.
If you are having any issues with it, my email is painyjames@gmail.com .


