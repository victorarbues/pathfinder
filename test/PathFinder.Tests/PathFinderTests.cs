﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace PathFinder.Tests
{
    public class PathFinderTests
    {

		private const string TestInput = "AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7";

		private readonly IPathFinder _pathFinder;

		public PathFinderTests()
		{
			_pathFinder = new PathFinder(TestInput);
		}

		[Fact]
		public async void The_ABC_Distance_Should_Be_9()
		{
			var paths = await _pathFinder.Paths("ABC");
			Assert.Equal(9, paths.First().Distance);
		}

		[Fact]
		public async void The_ABC_Distance_Should_Be_9_Direct_Path()
		{
			var paths = await _pathFinder.Paths("ABC", pathResolver: "direct");
			Assert.Equal(9, paths.First().Distance);
		}

		[Fact]
		public async void The_AEBCD_Distance_Should_Be_22()
		{
			var paths = await _pathFinder.Paths("AEBCD");
			Assert.Equal(22, paths.First().Distance);
		}

		[Fact]
		public async void The_AED_Has_No_Such_Route()
		{
			var paths = await _pathFinder.Paths("AED", pathResolver: "direct");
			Assert.True(!paths.Any());
		}

		[Fact]
		public async void Should_be_2_Paths_For_C_to_C_With_A_Maximum_Of_3_Stops()
		{
			var paths = await _pathFinder.Paths("C", maxStops: 3, pathResolver: "circular");
			Assert.Equal(2, paths.Count());
		}

		[Fact]
		public async void Should_be_3_Paths_For_A_To_C_With_Exactly_4_Stops()
		{
			var paths = (await _pathFinder.Paths("AC", maxStops: 4, pathResolver: "origindestination"));
			paths = paths.Where(p => p.Roads.Count == 4);
			Assert.Equal(3, paths.Count());
		}

		[Fact]
		public async void The_AC_Shortest_Distance_Should_Be_9()
		{
			var paths = await _pathFinder.Paths("AC");
			Assert.Equal(9, paths.First().Distance);
		}

		[Fact]
		public async void The_AC_Shortest_Distance_Should_Be_9_With_Djikstra()
		{
			var paths = await _pathFinder.Paths("AC", pathResolver: "dijkstra");
			Assert.Equal(9, paths.First().Distance);
		}

		[Fact]
		public async void The_AD_Shortest_Distance_Should_Be_5_With_Djikstra()
		{
			var paths = await _pathFinder.Paths("AD", pathResolver: "dijkstra");
			Assert.Equal(5, paths.First().Distance);
		}

		[Fact]
		public async void The_Shortest_Route_From_B_To_B()
		{
			var paths = await _pathFinder.Paths("B", maxStops: TestInput.Split(',').Count(), pathResolver: "circular");
			Assert.Equal(9, paths.First().Distance);
		}

		[Fact]
		public async void Should_be_7_Paths_For_C_to_C_With_A_Distance_Less_Than_30()
		{
			var paths = await _pathFinder.Paths("C", maxDistance: 30, pathResolver: "circularloop");
			Assert.Equal(7, paths.Count());
		}

	}

}
